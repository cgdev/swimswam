SwimSwam Framework Documentation
================================

Asynchronous message passing in a heterogenous language environment
-------------------------------------------------------------------

SwimSwam consists of two command.  'SWIM' executes a command in any language that has a provider and interpreter.  'SWAM' retrieves the returned values of 'SWIM' commands.  All values are converted to JSON upon being returned from the the language VM.  Native conversions are used if available.

Examples:

```
SWIM three:PYTHON x=[x for x in range(1..3)]
SWIM sean:PHP $sean= "Sean!"
SWIM cats:JS {'Fluffy': 'cute', 'Cuddles': 'snuggly', 'Schrodinger': 'maybe?'}
SWIM dogs:JAVA ArrayList<String> dogs = Arrays.asList(new String[]{"Barker", "Growlie", "Goldy"})
```

Returns:

```
SWAM three
/*
[1,2,3]
 */

SWAM sean
/*
["Sean!"]
 */

SWAM cats
/*
{
	"Fluffy": "cute",
	"Cuddles": "snuggly",
	"Schrodinger": "maybe?"
}
 */
```

SWIM commands are pre-processed for syntax correctness, and return values are implicly assigned.  Results are returned as deferreds that act like promises, allowing manipulation and chaining without having to resolve.

What Purpose Does This Serve?
-----------------------------

None whatsoever.  This has no practical application.